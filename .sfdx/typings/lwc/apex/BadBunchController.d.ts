declare module "@salesforce/apex/BadBunchController.generateRecords" {
  export default function generateRecords(param: {size: any}): Promise<any>;
}
declare module "@salesforce/apex/BadBunchController.generateJSONRecords" {
  export default function generateJSONRecords(param: {size: any}): Promise<any>;
}
declare module "@salesforce/apex/BadBunchController.runDescribe" {
  export default function runDescribe(): Promise<any>;
}
