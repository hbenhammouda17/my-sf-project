declare module "@salesforce/apex/CreateMixedRecordsApexControllerLwc.createContactAndOpportunity" {
  export default function createContactAndOpportunity(param: {contactFirstName: any, contactLastName: any, opportunityName: any}): Promise<any>;
}
