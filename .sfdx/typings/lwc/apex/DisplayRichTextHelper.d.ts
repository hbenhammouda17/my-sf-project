declare module "@salesforce/apex/DisplayRichTextHelper.generatePDF" {
  export default function generatePDF(param: {wrapperList: any}): Promise<any>;
}
