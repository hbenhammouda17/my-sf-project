declare module "@salesforce/apex/ListControllerLwc.getAccounts" {
  export default function getAccounts(): Promise<any>;
}
