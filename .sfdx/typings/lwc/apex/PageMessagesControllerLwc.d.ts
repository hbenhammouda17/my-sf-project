declare module "@salesforce/apex/PageMessagesControllerLwc.callApex" {
  export default function callApex(): Promise<any>;
}
