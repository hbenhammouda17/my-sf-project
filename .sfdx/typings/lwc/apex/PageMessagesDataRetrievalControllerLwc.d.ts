declare module "@salesforce/apex/PageMessagesDataRetrievalControllerLwc.getAccounts" {
  export default function getAccounts(): Promise<any>;
}
