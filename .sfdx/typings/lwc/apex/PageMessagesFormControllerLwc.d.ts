declare module "@salesforce/apex/PageMessagesFormControllerLwc.createCity" {
  export default function createCity(param: {cityName: any}): Promise<any>;
}
