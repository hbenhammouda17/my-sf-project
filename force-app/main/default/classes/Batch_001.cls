/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-13-2023
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
global with sharing class Batch_001 implements Database.Batchable<SObject>{
    public Batch_001() {

    }

    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            [SELECT Id, Name, Type FROM Account WHERE Name !='']
        );
    }

    global static void execute(){
        Batch_001 bat = new Batch_001();
        Database.executeBatch(bat);
    }

    global void execute(Database.BatchableContext bc, List<Account> accountList){
        System.debug('inside execute methode');
        for(Account acc : accountList){
            FutureClass.futureMethod(acc.Id);
        }
    }

    global void finish(Database.BatchableContext bc){}
}