/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-09-2023
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
global class ContactSoapData {
    
    webservice static String getContactName(String id){
        return [SELECT Name FROM Contact WHERE Id = :id].Name;
    }
}