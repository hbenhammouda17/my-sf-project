/**
 * @description       : 
 * @author            : Hemdene Ben Hammouda
 * @group             : 
 * @last modified on  : 11-29-2021
 * @last modified by  : Hemdene Ben Hammouda
**/
public with sharing class DisplayPDFController {

    public String displayText {get; set;}
    public String nameText {get; set;}
    public DisplayPDFController() {
        displayText = String.escapeSingleQuotes(
            ApexPages.currentPage().getParameters().get('displayText'));
        nameText = String.escapeSingleQuotes(
            ApexPages.currentPage().getParameters().get('nameText'));
    }
}